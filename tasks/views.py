from django.shortcuts import render, redirect, get_object_or_404
from tasks.forms import TaskForm
from django.contrib.auth.decorators import login_required
from tasks.models import Task

# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save()
            task.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {"form": form}
    return render(request, "tasks/create_task.html", context)


@login_required
def tasks_list(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {"tasks": tasks}
    return render(request, "tasks/tasks_list.html", context)

def task_detail(request, id):
    task = get_object_or_404(Task, id=id)
    context = {
        "task_object": task
    }
    return render(request, "tasks/task_detail.html", context)