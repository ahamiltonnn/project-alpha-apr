from django.urls import path
from tasks.views import create_task, tasks_list, task_detail

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", tasks_list, name="show_my_tasks"),
    path("<int:id>/", task_detail, name="task_detail"),
]
